﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator
{
    public class StringCalculator
    {
        public int Calculate(string arg)
        {
            int returnValue = 0;

            if (arg.Length == 0)
                return 0;

            //if (arg.Contains(",") & arg.Contains("\n"))
            string separator = "";
            string[] values;
            
            if (arg.Substring(0, 1) == "/" && arg.Substring(1, 1) == "/")
            {
                if (arg.Contains("[") && arg.Contains("]"))
                {
                    var leftBracketIdx = arg.IndexOf('[');
                    var rightBracketIdx = arg.IndexOf(']');
                    var length = rightBracketIdx - leftBracketIdx - 1;
                    separator = arg.Substring(leftBracketIdx + 1, length);
                }
                else
                {
                    separator = arg.Substring(2, 1);
                }
                var beginIndex = arg.IndexOf('\n');
                arg = arg.Substring(beginIndex + 1);
            }
            values = arg.Split(new string[] { ",", "\n", separator }, StringSplitOptions.None);

            if (values.Length == 1)
                returnValue = int.Parse(values[0]);
            else
                for (int i = 0; i < values.Length; ++i)
                {
                    var summand = int.Parse(values[i]);
                    if (summand < 0)
                        throw new ArgumentException("Negative number detected");
                    returnValue += summand <= 1000 ? summand : 0;
                }


            return returnValue;
        }
    }
}
